{assign var="seotitle" value=$product->seotitle}
{assign var="seokeywords" value=$product->seokeywords}
{assign var="seodescription" value=$product->seodescription}
{include file="header.tpl"}
    <header class="header_banner">
        <div class="container">
            <h1 class="head_h1">关于我们</h1>
        </div>
    </header>
    <div class="container">
        <div class="row page_path">
            <a href="#" class="visited_path">当前位置：</a>
            <a href="#" class="visited_path">首页></a>
            <a href="#" class="current_path">关于我们</a>
        </div>
    </div>

    <section class="page_section">
        <div class="container">
            <h2>关于我们</h2>
            <div class="row">
                <img src="img/about_us_banner.png" class="col-sm-12 animated rotateInUpRight" alt="">
            </div>
            <br>
            <br>
            <div class="row">
                <p class="col-lg-5 col-md-6 col-sm-12 url_p">
                    〝超快排"——快速排名就找超快排<br>www.seo691.com
                </p>
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <p class="prologue">
                        超快排，是一款非竞价网络推广平台，
                        已帮助近100万家企业解决了推广难题。
                        拥有一批资深互联网开发团队，
                        我们有12年经验的系统架构师和软件开发工程师，
                        10年经验的seo优化专家和大数据分析师，
                        还有8年致力于用户行为研究的用户心理学导
                        师以及专业的客服服务团队。
                    </p>
                    <hr>
                    <p class="prologue">
                        超快排，是专注研究网站关键词优化、
                        研究用户行为体验、研究网站关键词排名
                        提升的产品。长期和国内众多SEO研究
                        人员研究网站排名算法，确保用户网站
                        关键词排名优化效果更佳，并长期有效
                        、稳定、安全！
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <h2>发展历程</h2>
            <h3>每一步，成就新高度</h3>
            <div class="row">
                <ul class="col-lg-5 col-md-12 left_progress">
                {assign var="productlist" value=$productdata->TakeProductListByName(progress,0,10)}
                {foreach from=$productlist item=productinfo}
                    <li class="progress_card left_card_1">
                        <div class="row">
                            <img src="{$productinfo->thumb}" alt="" class="col-lg-4">
                            <div class="col-lg-8">
                                <span class="progress_year">{$productinfo->name}</span>
                                <span class="progress_declaration">{$productinfo->seodescription}</span>
                            </div>
                        </div>
                    </li>
                {/foreach}
                </ul>
                <div class="col-lg-2 col-3">
                    <img class="progress_img" src="{$siteurl}/templets/{$templets->directory}/img/progress.png" alt="">
                </div>
                <ul class="col-lg-5 col-9 progress_right">
                {assign var="productlist" value=$productdata->TakeProductListByName(progress,0,10)}
                {foreach from=$productlist item=productinfo}
                    <li class="progress_card">
                        <div class="row">
                            <div class="col-lg-8 col-sm-12">
                                <span class="progress_year_right">{$productinfo->name}</span>
                                <span class="progress_declaration_right">
                                    {$productinfo->seodescription}
                                </span>
                            </div>
                            <img src="{$productinfo->thumb}" alt="" class="col-lg-4 progress_photeo">
                        </div>
                    </li>
                    {/foreach}
                </ul>
            </div>
        </div>
    </section>


<!-- 新 Bootstrap4 核心 CSS 文件 -->
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
    <!-- bootstrap.bundle.min.js 用于弹窗、提示、下拉菜单，包含了 popper.min.js -->
    <script src="https://cdn.staticfile.org/popper.js/1.15.0/umd/popper.min.js"></script>
    <!-- 最新的 Bootstrap4 核心 JavaScript 文件 -->
    <script src="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{$siteurl}/templets/{$templets->directory}/css/animate.css"/>
    <link rel="stylesheet" href="{$siteurl}/templets/{$templets->directory}/css/about_us.css">
    <script src="{$siteurl}/templets/{$templets->directory}/js/nav_effect.js"></script>
{include file="footer.tpl"}
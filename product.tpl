{assign var="seotitle" value=$product->seotitle}
{assign var="seokeywords" value=$product->seokeywords}
{assign var="seodescription" value=$product->seodescription}
{include file="header.tpl"}

<header class="header_banner">
        <div class="container">
            <h1 class="head_h1">SEO优化服务</h1>
            <h2 class="head_h2">提升品牌可信度，知名度，让你的网站更具有竞争力、营销力</h2>
        </div>
    </header>
    <div class="container">
        <div class="row page_path">
            <a href="#" class="visited_path">当前位置：</a>
            <a href="#" class="visited_path">首页></a>
            <a href="#" class="current_path">产品介绍</a>
        </div>
    </div>
    <section class="page_section">
        <div class="container">
            <h2>“超快排”功能简介</h2>
            <div class="row">
                <p class="box_shadow prologue_content">
                    超快排是全国独家采用谷歌内核浏览器开发的seo优化软件。
                    软件可优化百度pc、百度手机、百度提权、神马uc、搜狗pc、
                    搜狗手机、必应、360等众多主流搜索引擎。提供免费网站分析，
                    新站速排，整站优化等服务。用户在线注册账号、智能操作、
                    1对1专人服务、方便、省心、快捷！
                </p>
            </div>

            {assign var="productlist" value=$productdata->TakeProductListByName("product_introduce",0,4)}
            {foreach from=$productlist item=productinfo}
            {if $productinfo->pid%2==0}
                <div class="row" style="margin-top: 150px;">
                    <img src="{$productinfo->thumb}" class="col-lg-6 col-md-12" alt="">
                    <div class="col-lg-6 col-md-12">
                        <h3 class="prologue_class_title">{$productinfo->name}</h3>
                        <h4 class="prologue_class_h4">{$productinfo->seotitle}</h4>
                        <p class="prologue_class_content">
                            {$productinfo->seodescription}
                        </p>
                    </div>
                </div>
            {else}
                <div class="row" style="margin-top: 150px;">
                    <div class="col-lg-6 col-md-12">
                        <h3 class="prologue_class_title">{$productinfo->name}</h3>
                        <h4 class="prologue_class_h4">{$productinfo->seotitle}</h4>
                        <p class="prologue_class_content">
                            {$productinfo->seodescription}
                        </p>
                    </div>
                    <img src="{$productinfo->thumb}" class="col-lg-6 col-md-12" alt="">
                </div>
            {/if}
            {/foreach}
        </div>
    </section>
    <section class="page_section" style="background-color: #F5F9FF;">
        <div class="container">
            <h2>优化内容</h2>
            <ul class="row">
                <li class="col-lg-3 col-md-6 col-sm-12">
                    <ul class="optimization_ul">
                    <li class="optimization_ul_title">技术优化分析</li>
                    {assign var="productlist" value=$productdata->TakeProductListByName("optimization_content_1",0,10)}
                    {foreach from=$productlist item=productinfo}
                        <li class="optimization_item">{$productinfo->name}</li>
                    {/foreach}
                    </ul>
                </li>
                <li class="col-lg-3 col-md-6 col-sm-12">
                    <ul class="optimization_ul">
                    <li class="optimization_ul_title">网络结构优化分析</li>
                    {assign var="productlist" value=$productdata->TakeProductListByName("optimization_content_1",0,10)}
                    {foreach from=$productlist item=productinfo}
                        <li class="optimization_item">{$productinfo->name}</li>
                    {/foreach}
                    </ul>
                </li>
                <li class="col-lg-3 col-md-6 col-sm-12">
                    <ul class="optimization_ul">
                    <li class="optimization_ul_title">关键词分析</li>
                    {assign var="productlist" value=$productdata->TakeProductListByName("optimization_content_1",0,10)}
                    {foreach from=$productlist item=productinfo}
                        <li class="optimization_item">{$productinfo->name}</li>
                    {/foreach}
                    </ul>
                </li>
                <li class="col-lg-3 col-md-6 col-sm-12">
                    <ul class="optimization_ul">
                    <li class="optimization_ul_title">关键词分析</li>
                    {assign var="productlist" value=$productdata->TakeProductListByName("optimization_content_1",0,10)}
                    {foreach from=$productlist item=productinfo}
                        <li class="optimization_item">{$productinfo->name}</li>
                    {/foreach}
                    </ul>
                </li>
            </ul>
        </div>
    </section>
    <section class="page_section">
        <div class="container">
            <h2>服务流程</h2>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-2 side_procedure">
                    <img src="img/ic_procedure_side.png" alt="">
                </div>
                <div class="col-lg-12 col-md-12 col-10">
                    <ul class="procedure_ul row">
                    {assign var="productlist" value=$productdata->TakeProductListByName("procedure",0,4)}
                    {foreach from=$productlist item=productinfo}
                        <li class="procedure_item col-lg-3 col-md-3 col-sm-12">
                            <img src="{$productinfo->thumb}" alt="">
                            <span>{$productinfo->name}</span>
                        </li>
                    {/foreach}
                    </ul>
                </div>
                <div class="bottom_procedure" 
                    class="col-lg-12 col-md-12" style="width: 100%;">
                    <img src="img/ic_procedure.png" alt="" style="width: 100%;">
                </div>
            </div>
        </div>
    </section>


<!-- 新 Bootstrap4 核心 CSS 文件 -->
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
    <!-- bootstrap.bundle.min.js 用于弹窗、提示、下拉菜单，包含了 popper.min.js -->
    <script src="https://cdn.staticfile.org/popper.js/1.15.0/umd/popper.min.js"></script>
    <!-- 最新的 Bootstrap4 核心 JavaScript 文件 -->
    <script src="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{$siteurl}/templets/{$templets->directory}/css/product.css">
    <script src="{$siteurl}/templets/{$templets->directory}/js/nav_effect.js"></script>
{include file="footer.tpl"}
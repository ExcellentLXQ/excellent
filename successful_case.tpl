{assign var="seotitle" value=$product->seotitle}
{assign var="seokeywords" value=$product->seokeywords}
{assign var="seodescription" value=$product->seodescription}
{include file="header.tpl"}

    <header class="header_banner">
        <div class="container">
            <h1 class="head_h1">SEO优化服务</h1>
            <h2 class="head_h2">提升品牌可信度，知名度，让你的网站更具有竞争力、营销力</h2>
        </div>
    </header>
    <div class="container">
        <div class="row page_path">
            <a href="#" class="visited_path">当前位置：</a>
            <a href="#" class="visited_path">首页></a>
            <a href="#" class="current_path">成功案例</a>
        </div>
    </div>

    <section class="page_section">
        <div class="container">
            <h2 class="section_title">成功案例</h2>
            <ul class="row">
            {assign var="productlist" value=$productdata->TakeProductListByName(case,0,100)}
            {foreach from=$productlist item=productinfo}
                <li class="col-lg-4 col-md-6 col-sm-12">
                    <div class="case_container">
                        <div class="case_img" style="background-image: url({$productinfo->thumb});">
                            <div class="case_item_mark">
                                <button class="btn_get_this">索要方案</button>
                            </div>
                        </div>
                        <span class="case_name">{$productinfo->name}</span>
                    </div>
                </li>
            {/foreach}
            </ul>
        </div>
    </section>


<!-- 新 Bootstrap4 核心 CSS 文件 -->
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
    <!-- bootstrap.bundle.min.js 用于弹窗、提示、下拉菜单，包含了 popper.min.js -->
    <script src="https://cdn.staticfile.org/popper.js/1.15.0/umd/popper.min.js"></script>
    <!-- 最新的 Bootstrap4 核心 JavaScript 文件 -->
    <script src="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{$siteurl}/templets/{$templets->directory}/css/successful_case.css">
    <script src="{$siteurl}/templets/{$templets->directory}/js/nav_effect.js"></script>
{include file="footer.tpl"}
{assign var="seotitle" value=$titlekeywords}
{assign var="seokeywords" value=$metakeywords}
{assign var="seodescription" value=$metadescription}
{include file="header.tpl"}
<header class="header_banner">
        <div class="container">
            <h1 class="head_h1">更好的明天，从这里起航</h1>
        </div>
    </header>
    <div class="bg_container">
        <div class="container">
            <div class="row page_path">
                <a href="#" class="visited_path">当前位置：</a>
                <a href="#" class="visited_path">首页></a>
                <a href="#" class="current_path">软件下载</a>
            </div>

            <section class="page_section">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <h3 class="download_title">超快排流量提升助手下载</h3>
                            <h4 class="download_subtitle">仅windows系统下载</h4>
                            <ul class="row">
                            {assign var="productlist" value=$productdata->TakeProductListByName(download,0,3)}
                            {foreach from=$productlist item=productinfo}
                                <li class="col-lg-3 col-md-12 download_way">
                                    <img src="{$productinfo->thumb}" alt="">
                                    <button class="page_btn">{$productinfo->name}</button>
                                    <small>{$productinfo->seodescription}</small>
                                </li>
                            {/foreach}
                            </ul>
                        </div>
                        <div class="col-lg-6 col-md-12 right_side">
                            <!-- <img src="img/download_right_side.png" alt=""> -->
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<!-- 新 Bootstrap4 核心 CSS 文件 -->
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
    <!-- bootstrap.bundle.min.js 用于弹窗、提示、下拉菜单，包含了 popper.min.js -->
    <script src="https://cdn.staticfile.org/popper.js/1.15.0/umd/popper.min.js"></script>
    <!-- 最新的 Bootstrap4 核心 JavaScript 文件 -->
    <script src="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{$siteurl}/templets/{$templets->directory}/css/download_page.css">
    <script src="{$siteurl}/templets/{$templets->directory}/js/nav_effect.js"></script>
{include file="footer.tpl"}
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ">
                    <ul class="space_between such_link">
                        <li><img src="img/8_ic_1.png" alt=""></li>
						{assign var="topnavlist" value=$navdata->TakeNavigateList("友情链接",0,100)}
                        {foreach from=$topnavlist item=navinfo}
							<li><a href="#">{$navinfo->name}</a></li>
                        {/foreach}
                    </ul>
                    <ul class="space_between nav_link bottom_nav" 
                        style="margin-bottom: 50px;margin-top: 50px;">
                        <li><img src="img/0_logo.png" alt=""></li>
						{assign var="topnavlist" value=$navdata->TakeNavigateList("顶部导航",0,100)}
                        {foreach from=$topnavlist item=navinfo}
                        <li><a href="#">{$navinfo->name}</a></li>
                    {/foreach}
                    </ul>
                </div>
                <div class="col-lg-2 offset-lg-1 bottom_effect">
                    <img src="{$siteurl}/templets/{$templets->directory}/img/8_qrcode.png" class=" qr_code_img" alt="">
                </div> 
            </div>
            <div class="row">
                <p class="copyright_info nav_link">
                    桂公网安备 xx000000号
                    广西简创网络技术有限公司 桂ICP备xx000号
                </p>
            </div>
        </div>
    </footer>
</body>

</html>
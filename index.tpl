{assign var="seotitle" value=$titlekeywords}

{assign var="seokeywords" value=$metakeywords}

{assign var="seodescription" value=$metadescription}

{include file="header.tpl"}

<div class="header_banner">
        <div class="container">
            <div class="row" style="padding-top: 50px;">
                <div class="col-md-6 banner_left_text">
                    <p>快速排名就找超快排</p>
                    <p>全新算法助力企业轻松获客</p>
                    <button class="page_btn">立即了解</button>
                </div>
                <div class="col-md-6 banner_right_img">
                    <img src="{$siteurl}/templets/{$templets->directory}/img/0_img_banner1_right.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <section class="container page_section">
        <h2>以用户利益为目标的关键词快速排名产品</h2>
        <h3>超快排提供从网站分析、网站优化方案、
            网站关键词查找到智能应用的全流程服务与方案，全新算法帮助企业快速引流获客</h3>
        <ul class="row fast_rank_product">
		{assign var="productlist" value=$productdata->TakeProductListByName("keywordProduct",0,4)}
		{foreach from=$productlist item=productinfo}
			<li class="col-lg-3 col-md-6 com-sm-12">
                <div>
                    <img src={$productinfo->thumb} class="product_img" alt="">
                    <span class="product_name">{$productinfo->name}</span><br>
                    <p class="product_introduce">{$productinfo->seodescription}</p>
                </div>
            </li>
		{/foreach}
        </ul>
    </section>
    <section class="container page_section">
        <h2>此刻你是否正面临一些难题</h2>
        <h3>超快排，网站SEO快速排名系统</h3>
        <div class="row bottom_linear_style">
            <div class="col-lg-6">
                <img class="difficult_img" src="{$siteurl}/templets/{$templets->directory}/img/2_img_1.png" alt="">
            </div>
            <div class="col-lg-6">
                <h4 class="difficult_title">新手刚入行做新站？急需排名没人教？SEO公司不懂选哪家？</h4>
                <p class="difficult_content">互联网世界那么大，SEO经验不足，排名优化的公司到底怎么选?没人教怎么办？
                    怎么做才能让网站快速有排名？</p>
                <img src="img/2_ic_2.png" class="symbol_img" alt="">
                <p class="symbol_content">“ 智能任务系统” 为您全方位优化网站，有一对一
                    专属客服，提供解决方案，排名持续上涨</p>
                <img src="img/2_ic_3.png" style="float: right;" alt="">
            </div>
        </div>
        <div class="row bottom_linear_style special_hidden">
            <div class="col-lg-6 col-md-12">
                <img class="difficult_img" src="{$siteurl}/templets/{$templets->directory}/img/2_img_2.png" alt="">
            </div>
            <div class="col-lg-6 col-md-12">
                <h4 class="difficult_title">新手刚入行做新站？急需排名没人教？SEO公司不懂选哪家？</h4>
                <p class="difficult_content">互联网世界那么大，SEO经验不足，排名优化的公司到底怎么选?没人教怎么办？
                    怎么做才能让网站快速有排名？</p>
                <img src="img/2_ic_2.png" class="symbol_img" alt="">
                <p class="symbol_content">“ 智能任务系统” 为您全方位优化网站，有一对一
                    专属客服，提供解决方案，排名持续上涨</p>
                <img src="img/2_ic_3.png" style="float: right;" alt="">
            </div>
        </div>
        <div class="row bottom_linear_style special_visible">
            <div class="col-lg-6 col-md-12">
                <h4 class="difficult_title">新手刚入行做新站？急需排名没人教？SEO公司不懂选哪家？</h4>
                <p class="difficult_content">互联网世界那么大，SEO经验不足，排名优化的公司到底怎么选?没人教怎么办？
                    怎么做才能让网站快速有排名？</p>
                <img src="img/2_ic_2.png" class="symbol_img" alt="">
                <p class="symbol_content">“ 智能任务系统” 为您全方位优化网站，有一对一
                    专属客服，提供解决方案，排名持续上涨</p>
                <img src="img/2_ic_3.png" style="float: right;" alt="">
            </div>
            <div class="col-lg-6 col-md-12">
                <img class="difficult_img" src="{$siteurl}/templets/{$templets->directory}/img/2_img_2.png" alt="">
            </div>
        </div>
        <div class="row bottom_linear_style">
            <div class="col-lg-6">
                <img class="difficult_img" src="{$siteurl}/templets/{$templets->directory}/img/2_img_3.png" alt="">
            </div>
            <div class="col-lg-6">
                <h4 class="difficult_title">新手刚入行做新站？急需排名没人教？SEO公司不懂选哪家？</h4>
                <p class="difficult_content">互联网世界那么大，SEO经验不足，排名优化的公司到底怎么选?没人教怎么办？
                    怎么做才能让网站快速有排名？</p>
                <img src="img/2_ic_2.png" class="symbol_img" alt="">
                <p class="symbol_content">“ 智能任务系统” 为您全方位优化网站，有一对一
                    专属客服，提供解决方案，排名持续上涨</p>
                <img src="img/2_ic_3.png" style="float: right;" alt="">
            </div>
        </div>
    </section>
    <section style="background-image: url({$siteurl}/templets/{$templets->directory}/img/3_img.png);">
        <div class="container page_ad">
            <p>
                定制全新优化方案<br>
                解决SEO优化排名问题
            </p>
            <small>customized new optimization solution to solve SEO
                optimization ranking problem</small>
        </div>
    </section>

    <section class="page_section">
        <h2>自主研发智能优势</h2>
        <h3>十年潜心研发SEO优化技术，专注各大搜索引擎优化效果</h3>
        <div class="container">
            <ul class="row feature_ul">
            {assign var="productlist" value=$productdata->TakeProductListByName("advantage",0,4)}
            {foreach from=$productlist item=productinfo}
                <li class="col-lg-4" style="margin-top: 50px;">
                    <div>
                        <img src="{$productinfo->thumb}" alt="">
                        <p>{$productinfo->name}</p>
                        <p>{$productinfo->content}</p>
                    </div>
                </li>
            {/foreach}
            </ul>
        </div>
    </section>
    <section style="margin-bottom:150px">
        <h2>选择超快排，让获客变得更简单</h2>
        <h3>因为专注，所以专业</h3>
        <div class="container">
            <ul class="carousel_btn_list space_between">
            {assign var="productlist" value=$productdata->TakeProductListByName("reason",0,5)}
            {foreach from=$productlist item=productinfo}
                <li>
                    <img src="{$productinfo->thumb}" alt=""><br>
                    <span>{$productinfo->name}</span>
                </li>
            {/foreach}
            </ul>
        </div>
        <div style="background-color: #F5F9FF;">
            <div class="container">
                <div class="row">
                    <p class="col-md-6 carousel_content_text">
                        完全真实有效优化，确保安全可靠，无后顾之忧，
                        稳定上首页，后期维护排名稳定更持久
                    </p>
                    <img class="col-md-6" src="img/5_img_1.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <section style="margin-bottom: 120px;">
        <h2>超快排-让精准流量提升20倍的排名软件</h2>
        <h3>打造搜索引擎排名优化行业的优秀运营商</h3>
        <div class="container">
            <ul class="row achievement_ul">
            {assign var="productlist" value=$productdata->TakeProductListByName(productivity,0,4)}
            {foreach from=$productlist item=productinfo}
                <li class="achievement_item col-md-6 col-lg-3 col-sm-12">
                    <p>{$productinfo->seotitle}</p>
                    <p>{$productinfo->name}</p>
                    <p>{$productinfo->seodescription}</p>
                </li>
            {/foreach}
            </ul>
        </div>
    </section>
    <section>
        <h2>超快排-我们服务的客户行业</h2>
        <h3>流量成本越来越高，我们给您低成本流量洼地</h3>
        <div class="container">
            <ul class="row customer_extend_ul">
            {assign var="productlist2" value=$productdata->TakeProductListByName("customer",0,6)}
		    {foreach from=$productlist2 item=productinfo2}
                <li class="col-lg-4 col-md-6 col-sm-12 ">
                    <div>
                        <div style="background-image: url({$productinfo2->thumb});">
                            <div class="customer_want_this">
                                <button>索要方案</button>
                            </div>
                        </div>
                        <p>{$productinfo2->name}</p>
                    </div>
                </li>
               {/foreach}
            </ul>
            <a href="#" class="get_more">查看更多>>></a><br>
        </div>
    </section>
    <section>
        <div class="container">
            <h2>优化排名，从现在开始</h2>
            <h3>快速提升网站关键词排名，让更多客户找到你</h3>
            <ul class="row immediately_ul">
            {assign var="productlist" value=$productdata->TakeProductListByName("optimization",0,6)}
            {foreach from=$productlist item=productinfo}
                <li class="col-lg-3 col-md-6 col-sm-12">
                    <div class="space_between">
                        <img class="" src={$productinfo->thumb} alt="">
                        <span class="">{$productinfo->name}</span>
                    </div>
                </li>
            {/foreach}
            </ul>
        </div>
    </section>
    <section style="background-image: url({$siteurl}/templets/{$templets->directory}/img/7_img.png);background-size: 100% 100%;">
        <div class="container footer_ad">
            <p>
                1,593,124 家企业都在用超快排关键词排名系统<br>
                快速提升网站关键词排名，你还在等什么？
            </p>
            <button class="page_btn">马上注册</button>
        </div>
    </section>
<div class="mobile_nav">
        <ul>
            <li>
                <img src="{$siteurl}/templets/{$templets->directory}/img/QQ_1.png" alt="">
                <span>QQ</span>
            </li>
            <li>
                <img src="{$siteurl}/templets/{$templets->directory}/img/contact_us.png" alt="">
                <span>电话</span>
            </li>
            <li style="position: relative;">
                <div class="bottom_middle">
                    <img src="{$siteurl}/templets/{$templets->directory}/img/index.png" alt="">
                    <span style="color: white;">首页</span>
                </div>
            </li>
            <li>
                <img src="{$siteurl}/templets/{$templets->directory}/img/register_login.png" alt="">
                <span>注册/登录</span>
            </li>
            <li>
                <img src="{$siteurl}/templets/{$templets->directory}/img/to_top.png" alt="">
                <span>
                    <a href="#">回到顶部</a>
                </span>
            </li>
        </ul>
    </div>
    <aside class="aside_fixed" id="aside_fixed">
        <div class="contact_way box_shadow" id="contact_way">
            <div>
                <span style="background-image: url({$siteurl}/templets/{$templets->directory}/img/ic_f_qq.png);">
                    立即交谈
                </span>
                <span style="background-image: url({$siteurl}/templets/{$templets->directory}/img/ic_f_phone.png);">
                    12345678901
                </span>
            </div>
            <img src="{$siteurl}/templets/{$templets->directory}/img/8_qrcode.png" alt="">
        </div>
        <div class="online_server" id="online_server">
            <img src="{$siteurl}/templets/{$templets->directory}/img/ic_f_kefu.png" alt="">
            在<br>线<br>客<br>服
        </div>
        <a href="#" class="back_top">
            <img src="{$siteurl}/templets/{$templets->directory}/img/ic_f_backtop.png" alt="">
        </a>
    </aside>

    <section class="hidden_mark" id="hidden_mark">
        
    </section>

    <section class="login_mark" id="login_mark">
        <div class="login_div">
            <form class="login_left behaviour_register">
                <div class="behaviour_title">
                    <span>注册</span>
                    <img src="{$siteurl}/templets/{$templets->directory}/img/close.png" class="close_behaviour_dialog" alt="">
                </div>
                <input type="text" name="" id="" required placeholder="手机号"
                    style="background-image: url({$siteurl}/templets/{$templets->directory}/img/ic_phone.png);">
                <input type="text" name="" id="" required placeholder="设置密码"
                    style="background-image: url({$siteurl}/templets/{$templets->directory}/img/ic_password.png);">
                <input type="text" name="" id="" required placeholder="短信验证码"
                    style="background-image: url({$siteurl}/templets/{$templets->directory}/img/ic_code.png);">
                <input type="text" name="" id="" required placeholder="联系QQ"
                    style="background-image: url({$siteurl}/templets/{$templets->directory}/img/ic_qq.png)">
                <button class="behaviour_register_btn">注册</button>
                <span class="behaviour_register_agree">
                    <input type="checkbox" name="" id="">
                    我已经同意
                    <a href="">《超快排服务协议》</a>
                </span>
            </form>
            <form class="login_left behaviour_login">
                <div class="behaviour_title">登录
                    <img src="{$siteurl}/templets/{$templets->directory}/img/close.png" class="close_behaviour_dialog" alt="">
                </div>
                <!-- Nav tabs -->
                <ul class="nav toggle_label" role="">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#home">手机短信</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#menu1">账号密码</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div id="home" class="container tab-pane active"><br>
                        <input type="text" class="form_input" name="" id="" required placeholder="手机号"
                            style="background-image: url({$siteurl}/templets/{$templets->directory}/img/ic_phone.png);">
                        <div class="verification_block">
                            <span>获取验证码</span>
                            <input type="text" class="form_input" name="" id="" required placeholder="短信验证码"
                            style="background-image: url({$siteurl}/templets/{$templets->directory}/img/ic_code.png);">
                        </div>
                        <button class="behaviour_register_btn">登录</button>
                    </div>
                    <div id="menu1" class="container tab-pane fade"><br>
                        <input type="text" class="form_input" name="" id="" required placeholder="手机号"
                            style="background-image: url({$siteurl}/templets/{$templets->directory}/img/ic_phone.png);">
                            <input type="text" class="form_input" name="" id="" required placeholder="密码"
                            style="background-image: url({$siteurl}/templets/{$templets->directory}/img/ic_password.png);">
                        <div class="verification_block">
                            <span>获取验证码</span>
                            <input type="text" class="form_input" name="" id="" required placeholder="短信验证码"
                            style="background-image: url({$siteurl}/templets/{$templets->directory}/img/ic_code.png);">
                        </div>
                        <button class="behaviour_register_btn">登录</button>
                    </div>
                </div>
            </form>
            <div class="login_right">
                <img src="{$siteurl}/templets/{$templets->directory}/img/ic_close.png" class="close_behaviour_dialog" alt="">
                <div class="behaviour_toggle_div">
                    <span class="redundant_1">已有账号<br>立即登录</span><br>
                    <small class="redundant_2">现在登陆，让排名起飞！</small><br>
                    <button class="behaviour_toggle_btn">登录</button>
                </div>
            </div>
        </div>
    </section>
    <!-- 新 Bootstrap4 核心 CSS 文件 -->
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
    <!-- bootstrap.bundle.min.js 用于弹窗、提示、下拉菜单，包含了 popper.min.js -->
    <script src="https://cdn.staticfile.org/popper.js/1.15.0/umd/popper.min.js"></script>
    <!-- 最新的 Bootstrap4 核心 JavaScript 文件 -->
    <script src="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{$siteurl}/templets/{$templets->directory}/css/index.css">
    <script src="{$siteurl}/templets/{$templets->directory}/js/index.js"></script>
{include file="footer.tpl"}
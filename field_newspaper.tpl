{assign var="seotitle" value=$titlekeywords}
{assign var="seokeywords" value=$metakeywords}
{assign var="seodescription" value=$metadescription}
{include file="header.tpl"}

<header class="header_banner">
        <div class="container">
            <h1 class="head_h1">SEO优化服务</h1>
            <h2 class="head_h2">提升品牌可信度，知名度，让你的网站更具有竞争力、营销力</h2>
        </div>
</header>
<div class="container">
        <div class="row page_path">
            <a href="#" class="visited_path">当前位置：</a>
            <a href="#" class="visited_path">首页></a>
            <a href="#" class="current_path">成功案例</a>
        </div>
    </div>
    <div class="container" style="margin-top: 50px;">
        <div class="row">
            <aside class="col-lg-4 col-md-12 ">
                <ul class="menu_side">
                    {assign var="newscat" value=$categorydata->GetCategoryList(0,'article',0,'')}
                    {foreach from=$newscat item=catinfo1}
                        <li class="menu_item">
                            <ul class="box_shadow menu_item_sub_menu">
                                <li class="class_item_title">
                                    <img src="{$siteurl}/templets/{$templets->directory}/img/ic_5.png" alt="">
                                    <a href="{formaturl type="category" siteurl=$siteurl name=$catinfo1->filename}">{$catinfo1->name}</a>
                                </li>
                                {assign var="productcatlist2" value=$categorydata->GetSubCategory(11,"11")}
                                {foreach from=$productcatlist2 item=catinfo2}
                                    <li class="menu_item_sub_name">{$catinfo2->name}</li>
                                {/foreach}
                            </ul>
                        </li>
                    {/foreach}
                </ul>
            </aside>
            <ul class="col-lg-8 col-md-12 article_list">
            {assign var="newslist" value=$articledata->TakeArticleListByName("new",0,100)}
            {foreach from=$newslist item=newsinfo}
                <li class="container">
                    <div class="row">
                        <div class="col-4" style="overflow: hidden;">
                            <img src="{$newsinfo->thumb}" alt="">
                        </div>
                        <div class="col-8 article_card">
                            <a target="_blank" href="{formaturl type="article" siteurl=$siteurl name=$newsinfo->filename}">{$newsinfo->title|truncate:"30":"..."}</a>
                            <p>
                                {$newsinfo->seodescription|truncate:"100":"..."}
                            </p>
                            <span>
                                浏览量: {$newsinfo->viewcount}
                            </span>
                        </div>
                    </div>
                </li>
            {/foreach}
            </ul>
        </div>
    </div>
    


<!-- 新 Bootstrap4 核心 CSS 文件 -->
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
    <!-- bootstrap.bundle.min.js 用于弹窗、提示、下拉菜单，包含了 popper.min.js -->
    <script src="https://cdn.staticfile.org/popper.js/1.15.0/umd/popper.min.js"></script>
    <!-- 最新的 Bootstrap4 核心 JavaScript 文件 -->
    <script src="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{$siteurl}/templets/{$templets->directory}/css/field_newpaper.css">
    <script src="{$siteurl}/templets/{$templets->directory}/js/nav_effect.js"></script>
    
{include file="footer.tpl"}
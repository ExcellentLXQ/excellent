<!DOCTYPE html>

<html>

<head>

<meta charset="utf-8"/>

<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />

<meta name="renderer" content="webkit" />

<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />

<title>{if $seotitle != $sitename}{$seotitle} - {/if}{$sitename}</title>

{if $seokeywords != "-" && $seokeywords != ""}

<meta name="keywords" content="{$seokeywords}" />

{/if}

{if $seodescription != "-" && $seodescription != ""}

<meta name="description" content="{$seodescription}" />

{/if}

<meta name="generator" content="YiqiCMS {$yiqi_cms_version}" />

<meta name="author" content="SEOWHY XIAOLIANG" />

<meta name="copyright" content="copyright 2015 seowhy.com all rights reserved." />

</head>
<body>
<nav class="my_nav" id="top">
        <div class="container navbar-expand-lg navbar-light">
            <button class="navbar-toggler my_toggler" type="button" data-toggle="collapse" data-target="#dynamic_div">
                <img src="{$siteurl}/templets/{$templets->directory}/img/more_.png" style="width: 30px;" alt="">
            </button>
            <div class="row">
                <a href="#" class="col-lg-2 col-md-12 col-sm-12">
                    <img src="{$siteurl}/templets/{$templets->directory}/img/0_logo.png" alt="">
                </a>
                <div class="col-lg-8 col-md-12">
                    <div class="collapse navbar-collapse" id="dynamic_div">
                        <ul class="dynamic_ul">
                            {assign var="topnavlist" value=$navdata->TakeNavigateList("顶部导航",0,100)}
                            {foreach from=$topnavlist item=navinfo}
                              <li><a target="_blank" href="{$navinfo->url}">{$navinfo->name}</a></li>
                            {/foreach}
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="register_box">
                        <span id="loginBtn">登录/</span>
                        <span id="registerBtn">注册</span>
                    </div>
                </div>
            </div>
        </div>
    </nav>
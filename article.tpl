{assign var="seotitle" value=$article->seotitle}

{assign var="seokeywords" value=$article->seokeywords}

{assign var="seodescription" value=$article->seodescription}

{include file="header.tpl"}

    <header>
        <div class="container">
            <h1 class="head_h1">SEO优化服务</h1>
            <h2 class="head_h2">提升品牌可信度，知名度，让你的网站更具有竞争力、营销力</h2>
        </div>
    </header>
    <div class="container">
        <div class="row page_path">
            <a href="#" class="visited_path">当前位置：</a>
            <a href="#" class="visited_path">首页></a>
            <a href="#" class="current_path">文章详情</a>
        </div>
    </div>
    <section class="container" style="margin-top: 50px;">
        <div class="row">
            <article class="col-lg-8 col-md-12">
                <h1 class="article_title">{$article->title}</h1>
                <div class="article_info">
                    <span>发布者：admin</span>
                    <span>发布时间：{$article->adddate}</span>
                    <span>浏览量: {$article->viewcount}</span>
                </div>
                <div class="abstract">
                    <img src="{$siteurl}/templets/{$templets->directory}/img/ic_1.png" alt="">
                    <img src="{$siteurl}/templets/{$templets->directory}/img/ic_2.png" alt="">
                    <img src="{$siteurl}/templets/{$templets->directory}/img/ic_3(2).png" alt="">
                    <img src="{$siteurl}/templets/{$templets->directory}/img/ic_4.png" alt="">
                    <p>
                        {$seodescription}
                    </p>
                </div>
                <p class="paragraph">
                    <img src="{$article->thumb}" alt="">
                </p>
                {$article->content}
            </article>
            <aside class="col-lg-4 col-md-12">
                <div>
                    <div class="aside_cloumn_title">
                        <span>推荐文章</span>
                    </div>
                    <ul class="aside_news_ul">
                    {assign var="newslist" value=$articledata->TakeArticleListByNamecom("news",0,100)}
                    {foreach from=$newslist item=newsinfo}
                        <li class="row">
                            <img src="{$newsinfo->thumb}" class="col-4" alt="">
                            <div class="col-8">
                                <a href="{formaturl type="article" siteurl=$siteurl name=$newsinfo->filename}">
                                    {$newsinfo->title}
                                </a><br>
                                <span>
                                    浏览量: {$newsinfo->viewcount}
                                </span>
                            </div>
                        </li>
                        {/foreach}
                    </ul>
                </div>
                <div style="margin-top: 100px;">
                    <div class="aside_cloumn_title">
                        <span>行业资讯</span>
                    </div>
                    <ul class="aside_news_ul">
                    {assign var="newslist" value=$articledata->TakeArticleListByName("news",0,100)}
                    {foreach from=$newslist item=newsinfo}
                        <li class="row">
                            <img src="{$newsinfo->thumb}" class="col-4" alt="">
                            <div class="col-8">
                                <a href="{formaturl type="article" siteurl=$siteurl name=$newsinfo->filename}">
                                    {$newsinfo->title}
                                </a><br>
                                <span>
                                    浏览量: {$newsinfo->viewcount}
                                </span>
                            </div>
                        </li>
                    {/foreach}
                    </ul>
                </div>
            </aside>
        </div>
    </section>


    <!-- 新 Bootstrap4 核心 CSS 文件 -->
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
    <!-- bootstrap.bundle.min.js 用于弹窗、提示、下拉菜单，包含了 popper.min.js -->
    <script src="https://cdn.staticfile.org/popper.js/1.15.0/umd/popper.min.js"></script>
    <!-- 最新的 Bootstrap4 核心 JavaScript 文件 -->
    <script src="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{$siteurl}/templets/{$templets->directory}/css/browse_news.css">
    <script src="{$siteurl}/templets/{$templets->directory}/js/nav_effect.js"></script>
    
{include file="footer.tpl"}
$(function () {
    $('#hidden_mark').css('height', window.innerHeight)
    $('#login_mark').css('height',window.innerHeight)
    $('#hidden_mark').click(function () {
        $('#contact_way').css("visibility","hidden")
        $('#hidden_mark').css("display","none")
    })
    $('#online_server').click(function () {
        let isVisibled = $('#contact_way').css("visibility")
        if (isVisibled == "visible") {
            $('#contact_way').css("visibility", "hidden")
            $('#hidden_mark').css("display", "none")
        } else {
            $('#contact_way').css("visibility", "visible")
            $('#hidden_mark').css("display", "block")
        }
    })
    $('.close_behaviour_dialog').click(function(){
        $('#login_mark').css('display','none')
    })
    $('#loginBtn').click(function (){
        $('#login_mark').css('display','block')
        toggleWay(0)
    })
    $('#registerBtn').click(function (){
        $('#login_mark').css('display','block')
        toggleWay(1)
    })

    $('.behaviour_toggle_btn').click(function(){
        if($(this).text()=='登录'){
            toggleWay(1)
            $(this).text('注册')
        }else{
            toggleWay(0)
            $(this).text('登录')
        }
    })
    $(window).scroll(function(){
        current=$(window).scrollTop()
        bannerHeight=$('.header_banner').height()-80
        if(current<bannerHeight){
            $('.my_nav').css('background','none')
        }else{
            $('.my_nav').css('backgroundColor','#468EFF')
        }
    })
    function toggleWay(flag){
        switch(flag){
            case 0:
                $('.login_left').css('z-index',16)
                $('.behaviour_login').css('display','block')
                $('.behaviour_register').css('display','none')
                //$('.behaviour_toggle_btn').text('注册')
                $('.redundant_1').html("已有账号<br>立即登陆！")
                $('.redundant_2').text('现在登陆，让排名起飞！')
                
                break
            case 1:
                $('.login_left').css('z-index',20)
                $('.behaviour_login').css('display','none')
                $('.behaviour_register').css('display','block')
                //$('.behaviour_toggle_btn').text('登录')
                $('.redundant_1').html("还没有账号？<br>立即免费注册！")
                $('.redundant_2').text('现在注册，让排名起飞！')
                break
        }
    }
})
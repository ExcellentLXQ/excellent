{assign var="seotitle" value=$product->seotitle}
{assign var="seokeywords" value=$product->seokeywords}
{assign var="seodescription" value=$product->seodescription}
{include file="header.tpl"}

    <header class="header_banner">
        <div class="container">
            <h1 class="head_h1">收费标准</h1>
    </header>
    <div class="container">
        <div class="row page_path">
            <a href="#" class="visited_path">当前位置：</a>
            <a href="#" class="visited_path">首页></a>
            <a href="#" class="current_path">收费标准</a>
        </div>
    </div>
    <section class="page_section">
        <div class="container">
            <h2>收费标准</h2>
            <div class="row">
                <div class="col-lg-3 col-md-12">
                    <div class="explain_card">
                        <div class="explain_card_title">
                            <img src="{$siteurl}/templets/{$templets->directory}/img/0.png" alt="">
                            <strong style="color: rgba(70, 142, 255, 1);">优化说明</strong>
                            <img src="{$siteurl}/templets/{$templets->directory}/img/0.png" alt="">
                        </div>
                        <p class="explain_card_content">
                            一个关键词优化1次，消耗是1积分，
                            1毛钱至少可优化5个关键词，
                            平均每月每词花费3元钱，
                            不及其他平台优化1天的价格。
                            超快排花最少的钱，做最牛的优化。
                        </p>
                        <button>立即注册</button>
                    </div>
                </div>
                <div class="col-lg-9 col-md-12">
                    <ul class="row">
                        {assign var="productlist" value=$productdata->TakeProductListByName("price",0,4)}
                        {foreach from=$productlist item=productinfo}
                        <li class="col-lg-3 col-md-6 col-sm-12 box_shadow">
                            <div class="meal_card_title">
                                <img src="{$productinfo->thumb}" class="discount_img" alt="">
                                {$productinfo->name}
                            </div>
                            <div class="meal_card_body">
                                <span>获得</span>
                                <strong class="meal_card_body_num">{$productinfo->seotitle}</strong>
                                <span>次优化</span>
                                <hr>
                            </div>
                            <div class="meal_card_footer">
                                <span>单价<mark>{$productinfo->seokeywords}</mark>分钱</span>
                                <span>(1积分=优化1次)</span>
                            </div>
                        </li>
                        {/foreach}
                    </ul>
                </div>
            </div>
        </div>
    </section>

<!-- 新 Bootstrap4 核心 CSS 文件 -->
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
    <!-- bootstrap.bundle.min.js 用于弹窗、提示、下拉菜单，包含了 popper.min.js -->
    <script src="https://cdn.staticfile.org/popper.js/1.15.0/umd/popper.min.js"></script>
    <!-- 最新的 Bootstrap4 核心 JavaScript 文件 -->
    <script src="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{$siteurl}/templets/{$templets->directory}/css/product_price.css">
    <script src="{$siteurl}/templets/{$templets->directory}/js/nav_effect.js"></script>
{include file="footer.tpl"}